var serverConfig = {};
// include modules
var express = require('express');
var HTTP = require('http');

// create objects from modules
var app = express();
var server = HTTP.Server(app);

// make app path accessable for the user
app.use('/', express.static(__dirname + '/app'));

// index path
app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');
});

server.listen(8080, function(){
    console.log('Listening on '+ server.address().port);
    console.log("open the app with: http://localhost:" + server.address().port + "/");
});
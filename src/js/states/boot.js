var Boot = {
};

Boot.init = function() {
    game.stage.backgroundColor = '#3A5963';
    game.renderer.renderSession.roundPixels = true;
    game.forceSingleUpdate = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

    // game.stage.disableVisibilityChange = true;

};

Boot.preload = function() {
    game.load.image('lets_go_to', 'assets/sprites/lets_go_to.png');
    game.load.spritesheet('the_mall', 'assets/sprites/the_mall.png', 252, 32, 2);
};

Boot.create = function(){

    game.state.start("LOAD");
};

Boot.update = function() {
};
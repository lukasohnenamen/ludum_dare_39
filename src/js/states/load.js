var Load = {
    assetsLoaded : false,
    animationDone : false,
    animationTimer : 1500
};

Load.init = function() {

};

Load.preload = function() {
    this.initAnimation();
    var self = this;
    window.setTimeout(function() {
        self.animationDone = true;
    }, this.animationTimer);
    game.load.bitmapFont('font', 'assets/fonts/font.png', 'assets/fonts/font.fnt');
    game.load.bitmapFont('font_green', 'assets/fonts/font_green.png', 'assets/fonts/font_green.fnt');
    game.load.bitmapFont('score_green', 'assets/fonts/score_green.png', 'assets/fonts/score_green.fnt');
    game.load.bitmapFont('score_white', 'assets/fonts/score_white.png', 'assets/fonts/score_white.fnt');
    game.load.image('path', 'assets/sprites/path.png');
    game.load.image('seat', 'assets/sprites/seat.png');
    game.load.image('cloth', 'assets/sprites/cloth.png');
    game.load.image('register', 'assets/sprites/register.png');
    game.load.spritesheet('girlfriend', 'assets/sprites/girlfriend.png', 32, 32, 4);
    game.load.spritesheet('player', 'assets/sprites/player.png', 32, 32, 10);
    game.load.spritesheet('bag', 'assets/sprites/bags.png', 32, 32, 6);
    game.load.spritesheet('person', 'assets/sprites/random_guy.png', 32, 32, 12);

    game.load.spritesheet('cloth_particles', 'assets/sprites/cloth_particles.png', 16, 16, 4);
    game.load.image('dust', 'assets/sprites/dust.png');
    game.load.image('exclamation_mark', 'assets/sprites/exclamation_mark.png');

    game.load.spritesheet('bar', 'assets/sprites/bar.png', 16, 1, 2);
    game.load.image('bar_bg', 'assets/sprites/bar_bg.png');
};

Load.initAnimation = function() {

    this.lets_go_to = game.add.sprite(game.world.width/2 - 125, -16, 'lets_go_to');
    this.the_mall = game.add.sprite(game.world.width/2, 144, 'the_mall');
    this.the_mall.animations.add('blink', [0, 0, 1, 0, 1]);
    this.the_mall.scale.setTo(0);
    this.the_mall.alpha = 0;
    this.the_mall.anchor.setTo(0.5);
    game.add.tween(this.lets_go_to.position).to( { y: 116, x : game.world.width/2 - 125 }, 300, Phaser.Easing.Cubic.In, true);
    game.time.events.add(300 , function() { 
        game.camera.shake(0.005, 300);
        game.add.tween(this.the_mall.scale).to( {x: 1, y : 1}, 300, Phaser.Easing.Quadratic.In, true );
        game.add.tween(this.the_mall).to( {alpha : 1}, 300, Phaser.Easing.Cubic.In, true );
        game.time.events.add(300, function() {
            game.camera.shake(0.01, 400);
            this.the_mall.animations.play('blink', 12);
        }, this);
    }, this);
    
    //var tween = 
    // tween.repeat(10, 100);
}

Load.animationOut = function() {
    game.add.tween(this.lets_go_to.position).to({ x : - 256 }, 300, Phaser.Easing.Quadratic.In, true);
    game.add.tween(this.lets_go_to.scale).to({ x : 1.2, y : 0.8 }, 100, Phaser.Easing.Quadratic.In, true);
    
    game.add.tween(this.the_mall.position).to({ x : - 256 }, 300, Phaser.Easing.Quadratic.In, true);
    game.add.tween(this.the_mall.scale).to({ x : 1.2, y : 0.8 }, 100, Phaser.Easing.Quadratic.In, true);
    game.time.events.add(500, function() {
        game.state.start("TUTORIAL");
    });
}

Load.create = function(){
    this.assetsLoaded = true;
}

Load.update = function() {
    if(this.assetsLoaded  && this.animationDone) {
        this.assetsLoaded = false;
        Load.animationOut();
    }
};


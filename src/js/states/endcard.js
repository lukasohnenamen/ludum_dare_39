var Endcard = {

};

Endcard.init = function() {

}

Endcard.create = function() {
    
    var highScore = (game.score.score() > game.score.highScore());

    var seat = game.add.sprite(game.world.width/2, 224, 'seat');
    seat.anchor.setTo(0.5);

    var player = game.add.sprite(game.world.width/2, 224, 'player');
    player.anchor.setTo(0.5);
    player.animations.add('sit', [6, 7, 8, 9, 10]);
    player.animations.play('sit', 8, true);

    var tScore = game.add.bitmapText(game.world.width/2-1, 48, 'font', 'Your Score', 8);
    tScore.anchor.setTo(0.5);

    if(highScore) {
        var tScoreNumber = game.add.bitmapText(game.world.width/2, 72, 'score_green', game.score.score(), 16);
        tScoreNumber.anchor.setTo(0.5);
        tScoreNumber.position.x = Math.floor(tScoreNumber.position.x);
        tScoreNumber.position.y = Math.floor(tScoreNumber.position.y);

        var tNewBest = game.add.bitmapText(game.world.width/2, 32, 'font_green', 'New Best!!', 8);
        tNewBest.anchor.setTo(0.5);
        tNewBest.scale.setTo(0, 0);
        game.time.events.add(400, function() {
            game.add.tween(tNewBest.scale).to( { x: 1, y : 1 }, 500, Phaser.Easing.Bounce.Out, true);
        });
    }
    else {
        var tScoreNumber = game.add.bitmapText(game.world.width/2, 72, 'score_white', game.score.score(), 16);
        tScoreNumber.anchor.setTo(0.5);
        tScoreNumber.position.x = Math.floor(tScoreNumber.position.x);
        tScoreNumber.position.y = Math.floor(tScoreNumber.position.y);
    }

    var tHighScore = game.add.bitmapText(game.world.width/2, 100, 'font', 'Personal Best', 8);
    tHighScore.anchor.setTo(0.5);

    var stringHighScore = (game.score.highScore() > 0) ? game.score.highScore()+'' : '--';

    var tHighScoreNumber = game.add.bitmapText(game.world.width/2, 116, 'font', stringHighScore, 8);
    tHighScoreNumber.anchor.setTo(0.5);
    
    if(highScore) {
        game.score.setHighScore( game.score.score() );
    }

    game.time.events.add(800,function() {
        var start = game.add.bitmapText(game.world.width/2, game.world.height - 32, 'font', 'press any key to play again...', 8);
        start.anchor.setTo(0.5);
        start.alpha = 0;
        var tween = game.add.tween(start).to( { alpha: 1 }, 500, "Linear", true, 0, -1, true);
        game.input.keyboard.onPressCallback = function(e){
            game.input.keyboard.onPressCallback = null;
            game.state.start("PLAY");
        }
    });
}


Endcard.update = function() {

}

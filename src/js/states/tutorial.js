var Tutorial = {
};

Tutorial.init = function() {
};

Tutorial.preload = function() {

};

Tutorial.create = function(){
    var movement = game.add.bitmapText(32, 32, 'font', 'use \'A\'/\'D\' to move', 8);
    var dodge = game.add.bitmapText(32, 96, 'font', 'dodge this guy with \'S\'', 8);
    var person = game.add.sprite(512-128, 80, 'person');
    person.animations.add('idle', [0, 1, 2, 3, 4, 5, 6, 7, 8]);
    person.animations.play('idle', 12, true);
    var sit = game.add.bitmapText(32, 160, 'font', 'sit down with \'S\' to refill your POWER', 8);
    var seat = game.add.sprite(512-128, 128+16, 'seat');
    var power = game.add.bitmapText(32, 224, 'font', 'never run out of POWER and carry \nas many bags as you can!!', 8);
    var powerBar = game.add.sprite(512-120, 200+64, 'bar');
    var powerBarBG = game.add.sprite(512-120, 200, 'bar_bg');
    powerBar.anchor.setTo(0, 1);
    powerBar.scale.setTo(1, 48);
    var start = game.add.bitmapText(game.world.width/2, game.world.height - 32, 'font', 'press any key to start...', 8);

    start.anchor.setTo(0.5);

    var tween = game.add.tween(start).to( { alpha: 0 }, 500, "Linear", true, 0, -1, true);

    game.input.keyboard.onPressCallback = function(e){
        game.input.keyboard.onPressCallback = null;
        game.state.start("PLAY");
    }
};

Tutorial.update = function() {};
var Play = {
    // layes
    backgroundLayer : null,
    objectLayerBack : null,
    groundLayer : null,
    objectLayerFront : null,
    foregroundLayer : null,
    vfxLayer : null,
    uiLayer : null,

    girlfriendSprite : null,
    playerSprite : null,

    worldController : null,
    playerController : null,
    girlfriendController : null,
    animationController : null,
    particleController : null,
    scoreController : null,
    uiController : null
}

Play.init = function() {
};  

Play.preload = function() {
    this.backgroundLayer = game.add.group();
    this.objectLayerBack = game.add.group();
    this.groundLayer = game.add.group();
    this.objectLayerFront = game.add.group();
    this.foregroundLayer = game.add.group();
    this.vfxLayer = game.add.group();
    this.uiLayer = game.add.group();

    this.girlfriendSprite = game.add.sprite(400, game.world.height - 96, 'girlfriend');
    this.playerSprite = game.add.sprite(100, game.world.height - 96, 'player');
    this.playerSprite.anchor.setTo(0.5, 0);

    this.worldController = Instancer.instanciate("WorldController", WorldController, { background : this.backgroundLayer, objectBackground: this.objectLayerBack, objectMiddleground : this.groundLayer, objectForeground : this.objectLayerFront });
    this.playerController = Instancer.instanciate("PlayerController", PlayerController, { player : this.playerSprite,  seats: this.worldController.getSeats(), persons : this.worldController.getPersons() });
    this.girlfriendController = Instancer.instanciate("GirlfriendController", GirlfriendController, { sprite : this.girlfriendSprite, clothStands : this.worldController.getClothStands() });
    this.animationController = Instancer.instanciate("AnimationController", AnimationController, { player : this.playerSprite, girlfriend : this.girlfriendSprite, persons : this.worldController.getPersons(), vfxLayer : this.vfxLayer });
    this.particleController = Instancer.instanciate("ParticleController", ParticleController, { girlfriend : this.girlfriendSprite, player : this.playerSprite, vfxLayer : this.vfxLayer });
    this.scoreController = Instancer.instanciate("ScoreController", ScoreController, {});
    this.uiController = Instancer.instanciate("UiController", UiController, { playerData : this.playerController.getModel(), uiLayer : this.uiLayer, scoreData : this.scoreController.getModel() });

    this.groundLayer.add(this.playerSprite);

    var cloth = game.add.sprite(250, game.world.height - 96, "cloth");
    this.objectLayerFront.add(cloth);
};

Play.create = function() {
};

Play.update = function() {
    this.playerController.update();
    this.girlfriendController.update();
    var speed = this.girlfriendController.getSpeed();
    var dt = game.time.elapsedMS/1000;
    var velocity = speed*dt;
    this.worldController.update(velocity);
    this.scoreController.update(velocity);
    this.particleController.update();
    this.uiController.update();

};

Play.shutdown = function() {
    this.worldController.shutDown();
    this.playerController.shutDown();
    this.girlfriendController.shutDown();
    this.animationController.shutDown();
    this.particleController.shutDown();
    this.scoreController.shutDown();
    this.uiController.shutDown();
};
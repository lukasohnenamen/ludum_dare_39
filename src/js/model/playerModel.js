var PlayerModel = function() {
    var self = this;

    var _power = 0;
    {
        self.setPower = function(power) {
            _power = power;
            if(_power > 100) _power = 100;
            if(_power < 0) _power = 0;
        }
        self.power = function() {
            return _power;
        }
    }
}
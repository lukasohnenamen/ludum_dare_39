var ScoreModel = function(){
    var self = this;

    var _score = 0;
    {
        self.addScore = function(score) {
            _score += score;
        }
        self.setScore = function(score) {
            _score = score;
        }
        self.score = function() {
            return Math.floor(_score);
        }
    }

    var _bags = 0; 
    {
        self.setBags = function(bags) {
            _bags = bags;
        }
        self.bags = function() {
            return _bags;
        }
    }

    var _highScore = 0;
    {
        self.setHighScore = function(score) {
            _highScore = score;
        }
        self.highScore = function() {
            return _highScore;
        }   
    }
}
var GirlfriendModel = function() {
    var self = this;

    var _speed = 1;
    {
        self.setSpeed = function(speed) {
            _speed = speed;
        }
        self.speed = function() {
            return _speed;
        }
    }
}
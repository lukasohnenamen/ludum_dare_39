/**
 * EventManager
 * register and trigger events 
 * with this util class
 */
var EventManager = {
    nextEventId : 0,
    eventListener : {}
};

EventManager.register = function(_eventKey, _callback) {
    ++this.nextEventId;
    if( this.eventListener[_eventKey] === undefined ) {
        this.eventListener[_eventKey] = [];
    }
    var newListener = {
        id : this.nextEventId,
        func : _callback
    };
    this.eventListener[_eventKey].push(newListener);
    return this.nextEventId;
};

EventManager.remove = function(_key, _listener) {
    var id = _listener.id || _listener;
    this.eventListener[_key] = this.eventListener[_key].filter(function(listener) {
        return listener.id !== id; 
    });
};

EventManager.removeAll = function(_key) {
    this.eventListener[_key] = [];
};

EventManager.trigger = function(_eventKey, _params) {

    var listeners = this.eventListener[_eventKey];
    if(listeners === undefined) return;
    listeners.forEach( function(listener) {
        listener.func(_params);
    });
};
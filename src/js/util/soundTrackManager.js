var SoundTrackManager = {
    tracks : {}
};

SoundTrackManager.addTrack = function(name, volume) {
    this.tracks[name] = {
        volume : volume,
        audio : {}
    }
}

SoundTrackManager.addSound = function(track, soundKey ) {
    this.tracks[track].audio[soundKey] =  game.add.audio(soundKey);
}

SoundTrackManager.play = function(track, soundKey) {
    this.tracks[track].audio[soundKey].play();
}

SoundTrackManager.setVolume = function(track, volume) {
    var sounds = this.tracks[track].audio;
    this.tracks[track].volume = volume;
    for (var sound in sounds) {
        sounds[sound].volume = this.tracks[track].volume;
    }
}
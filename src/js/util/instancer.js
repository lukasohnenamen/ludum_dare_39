var Instancer = {
    pool : {}
};


Instancer.instanciate = function(classKey, class, params) {
    if(!this.pool[classKey]) {
        this.pool[classKey] = new class();
    }
    this.pool[classKey].init(params);
    return this.pool[classKey];
};
var ScoreController = function() {
    var self = this;

    var _model = null;

    var _multiplicator = 1;

    var _levelUpEvent = null;

    self.init = function(params) {
        if(!_model) {
            _model = new ScoreModel();
            game.score = _model;
        }
        _model.setScore(0);
        _model.setBags(0);
        _multiplicator = Progress.scoreMultiplicator(1);

        _levelUpEvent = EventManager.register("LEVEL_UP", onLevelUp);
    }

    self.shutDown = function() {
        EventManager.remove("LEVEL_UP", _levelUpEvent);
    }

    self.update = function(velocity) {
        _model.addScore(velocity * _multiplicator);
    }

    self.getModel = function() {
        return _model;
    }

    function onLevelUp(params) {
        _multiplicator = Progress.scoreMultiplicator(params.level);
        _model.setBags(params.level - 1);
    }
}
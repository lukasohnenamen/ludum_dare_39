var GirlfriendController = function() {
    var self = this;

    var _sprite

    var _model = null;

    var _levelUpEvent = null;
    var _deathEvent = null;

    // collidables
    var _clothStands = null;

    self.init = function(params) {
        if(!_model) {
            _model = new GirlfriendModel();
        }
        _model.setSpeed( Progress.speed(1) );

        _clothStands = params.clothStands;
        _sprite = params.sprite;

        _levelUpEvent = EventManager.register("LEVEL_UP", onLevelUp);
        _deathEvent = EventManager.register("DEATH", onDeath);
    }

    self.update = function() {
        for(var i=0; i < _clothStands.length; ++i) {
            if(_clothStands[i].alive && !_clothStands[i].onGirlfriend) {
                var boundsA = _clothStands[i].getBounds();
                var boundsB = _sprite.getBounds();

                if(Phaser.Rectangle.intersects(boundsA, boundsB)) {
                    _clothStands[i].onGirlfriend = true;
                    EventManager.trigger('CLOTH_COLLISION', {type : "START", elem : _clothStands[i].objectID});
                }
            }
            else if(_clothStands[i].onGirlfriend) {
                var boundsA = _clothStands[i].getBounds();
                var boundsB = _sprite.getBounds();

                if(!Phaser.Rectangle.intersects(boundsA, boundsB)) {
                    _clothStands[i].onGirlfriend = false;
                    EventManager.trigger('CLOTH_COLLISION', {type : "STOP", elem : _clothStands[i].objectID});
                }
            }
        }
    }

    self.shutDown = function() {
        EventManager.remove("LEVEL_UP", _levelUpEvent);
        EventManager.remove("DEATH", _deathEvent);
    }

    self.getSpeed = function() {
        return _model.speed();
    }

    function onLevelUp(params) {
        _model.setSpeed( Progress.speed(params.level) );
    }

    function onDeath(params) {
        _model.setSpeed(0);
    }
}
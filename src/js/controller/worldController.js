var WorldController = function() {

    var self = this;

    var _currentLevel;

    var _background;
    var _objectBackground;
    var _objectMiddleground;
    var _objectForeground;

    var _maxLeftOffset = -64;

    var _numGroundTiles = 10;
    var _numSeats = 10;
    var _numPersons = 10;
    var _numClothStands = 5;

    var _groundTiles;
    var _seats;
    var _persons;
    var _clothStandsBack;
    var _clothStandsFront;
    var _register;

    var _levelTime = 10000;
    var _levelTimer = 10000;

    var _nextSeatTimeMin = 1300;
    var _nextSeatTimeMax = 3800;
    var _nextSeat = 1000;

    var _personSpawnMin = 0;
    var _personSpawnMax = 0;
    var _nextPerson = 0;

    var _nextClothMin = 1500;
    var _nextClothMax = 5000;
    var _nextCloth = 0;

    self.init =  function(params) {
        _currentLevel = 1;
        _background = params.background;
        _objectBackground = params.objectBackground;
        _objectMiddleground = params.objectMiddleground;
        _objectForeground = params.objectForeground;

        var i;
        _groundTiles = [];
        for(i=0; i < _numGroundTiles; ++i) {
            var sprite = game.add.sprite(i*127, game.world.height - 64, 'path');
            _groundTiles.push(sprite);
            _objectMiddleground.add(sprite);
        }

        _seats = _objectBackground.createMultiple(_numSeats, 'seat', 0, false);

        _persons = _objectForeground.createMultiple(_numPersons, 'person', 0 , false);
        _personSpawnMin = Progress.personSpawnMin(1);
        _personSpawnMax = Progress.personSpawnMax(1);
        _nextPerson = game.rnd.integerInRange(_personSpawnMin, _personSpawnMax);

        _clothStandsBack = _objectBackground.createMultiple(_numClothStands, 'cloth', 0, false);
        _clothStandsFront = _objectForeground.createMultiple(_numClothStands, 'cloth', 0, false);
        _nextCloth = game.rnd.integerInRange(_nextClothMin, _nextClothMax);
        for(var i=0; i < _numClothStands; ++i) {
            _clothStandsBack[i].objectID = i;
        }
        for(var i=0; i < _numClothStands; ++i) {
            _clothStandsFront[i].objectID = i + _numClothStands;
        }

        _register = game.add.sprite(0, 0, 'register');
        _register.upChecked = true;
        _register.kill();
        _objectBackground.add(_register);

        _levelTimer = _levelTime;
    }

    self.shutDown = function() {

    }

    self.update = function(velocity) {
        updateObjectLayer(_objectBackground, velocity);
        updateObjectLayer(_objectMiddleground, velocity);
        updateObjectLayer(_objectForeground, velocity);
       
        _nextSeat -= game.time.elapsedMS;
        _nextPerson -= game.time.elapsedMS;
        _nextCloth -= game.time.elapsedMS;
        _levelTimer -= game.time.elapsedMS;

        if(_nextSeat <= 0) {
            addSeat();
            _nextSeat = game.rnd.integerInRange(_nextSeatTimeMin, _nextSeatTimeMax);
        }
        if(_nextPerson <= 0) {
            addPerson();
            _nextPerson = game.rnd.integerInRange(_personSpawnMin, _personSpawnMax);
        }
        if(_nextCloth <= 0) {
            addClothStand();
            _nextCloth = game.rnd.integerInRange(_nextClothMin, _nextClothMax);
        }
        if(_levelTimer <= 0) {
            _register.reset(game.world.width + 64, game.world.height - 96);
            _register.upChecked = false;
            _levelTimer = _levelTime;
        }
        if(_register.alive === true && !_register.upChecked && _register.position.x <= game.world.width - 128) {
            _register.upChecked = true;
            ++_currentLevel;
            EventManager.trigger("LEVEL_UP", { level: _currentLevel});

            _nextSeatTimeMin
            _nextSeatTimeMax
        }
    }

    self.getSeats = function() {
        return _seats;
    }

    self.getPersons = function() {
        return _persons;
    }

    self.getClothStands = function(){
        return _clothStandsBack.concat(_clothStandsFront);
    }

    function addSeat() {
        var item = getFirstDead(_seats);
        if(item) {
            item.reset(game.world.width + 64,  game.world.height - 96 );
            if(_register.alive) {
                var boundsA = item.getBounds();
                var boundsB = _register.getBounds();

                if(Phaser.Rectangle.intersects(boundsA, boundsB)) {
                    item.position.x += 128;
                }
            }
        }
    }

    function addPerson() {
        var item = getFirstDead(_persons);
        if(item) {
            item.playerCollision = false;
            item.reset(game.world.width + 64,  game.world.height - 96 );
        }
    }

    function addClothStand() {
        var item;
        var layer = game.rnd.integerInRange(0, 1);
        if(layer === 0) {
            item = getFirstDead(_clothStandsBack);
        }
        else {
            item = getFirstDead(_clothStandsFront);
        }
        if(item) {
            item.reset(game.world.width + 64,  game.world.height - 96 );
        }
    }

    function getFirstDead(array) {
        for(var i=0; i < array.length; ++i) {
            if(array[i].alive === false) return array[i];
        }
        return null;
    }

    function updateObjectLayer(layer, velocity) {
        layer.forEach(function(item) {
            item.position.x -= velocity;
            if(item.position.x + item.width <= _maxLeftOffset) {
                resetItem(item);
            }
        });
    }

    function resetItem(item) {
        if(item.key === 'path') {
            item.x += 127*_numGroundTiles;
        }
        else {
            item.kill();
        }
    }
}
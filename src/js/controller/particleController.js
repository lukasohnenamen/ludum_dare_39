var ParticleController = function() {
    var self = this;

    var _clothEmitter = null;
    var _dustEmitter = null;
    var _dustEmitterOffset = -8;
    var _walking = true;
    var _dustEmittingBefore = false;

    var _girlfriendSprite;
    var _playerSprite;

    var _clothCollisions;

    var _clothCollisionEvent = null;
    var _directionChagneEvent = null;
    var _stateChangeEvent = null;

    self.init = function(params) {
        _clothCollisions = [];
        _girlfriendSprite = params.girlfriend;
        _playerSprite = params.player;

        _clothEmitter = game.add.emitter(_girlfriendSprite.position.x + 16, _girlfriendSprite.position.y, 40);
        _clothEmitter.makeParticles("cloth_particles", [0, 1, 2, 3]);
        _clothEmitter.setRotation(-360, 0);
        _clothEmitter.setXSpeed(-150, -50);
        _clothEmitter.setYSpeed(-100, -50);
        _clothEmitter.start(false, 3000, 200, 0);
        _clothEmitter.on = false;

        _dustEmitter = game.add.emitter(0, _playerSprite.position.y + 32, 100);
        _dustEmitter.makeParticles("dust");
        _dustEmitter.setRotation(0, 0);
        _dustEmitter.setYSpeed(-60, -10);
        _dustEmitter.alpha = 0.5;
        _dustEmitter.start(false, 300, 50, 0);
        _dustEmitter.on = false;

        _clothCollisionEvent = EventManager.register('CLOTH_COLLISION', onClothCollision);
        _directionChagneEvent = EventManager.register('DIRECTION_CHANGED', onDirectionChange);
        _stateChangeEvent = EventManager.register("NEW_PLAYER_STATE", onPlayerState);
    }

    self.update = function() {
        _dustEmitter.position.x = _playerSprite.position.x + _dustEmitterOffset;
    }

    self.shutDown = function() {
        EventManager.remove('CLOTH_COLLISION', _clothCollisionEvent);
        EventManager.remove('DIRECTION_CHANGED', _directionChagneEvent);
        EventManager.remove('NEW_PLAYER_STATE', _stateChangeEvent);
    }

    function startClothEmitter() {
        if(!_clothEmitter.on) {
            _clothEmitter.on = true;
        }
    }

    function stopClothEmitter() {
        if(_clothEmitter.on) {
            _clothEmitter.on = false;

        }
    }

    function onClothCollision(params) {
        if(params.type === "START") {
            if(!_clothCollisions.includes(params.elem)) {
                _clothCollisions.push(params.elem);
            }
            else {
            }
            if(_clothCollisions.length > 0) {
                startClothEmitter();
            }
        }
        else {
            var index = _clothCollisions.indexOf(params.elem);
            if (index > -1) {
                _clothCollisions.splice(index, 1);
            }
            if(_clothCollisions.length === 0) {
                stopClothEmitter();
            }
        }
    }

    function onDirectionChange(params) {
        if(_walking) {
            if(params.direction === "IDLE") {
                _dustEmitter.on = false;
            }
            else if(params.direction === "RIGHT") {
                _dustEmitterOffset = -8;
                _dustEmitter.setXSpeed(-60, -10);
                _dustEmitter.on = true;
            }
            else if(params.direction === "LEFT") {
                _dustEmitterOffset = 8;
                _dustEmitter.setXSpeed(10, 60);
                _dustEmitter.on = true;
            }
        }
    }

    function onPlayerState(params) {
        if(params.state === "WALKING") {
            _walking = true;
            if(_dustEmittingBefore) {
                _dustEmitter.on = true;
                _dustEmittingBefore = false;
            }
        }
        else {
            _walking = false;
            _dustEmittingBefore = _dustEmitter.on;
            _dustEmitter.on = false;
        }
    }   
}
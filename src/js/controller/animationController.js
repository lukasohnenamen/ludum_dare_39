var AnimationController = function() {
    var self = this;

    var _vfxLayer = null;

    var _playerSprite = null;
    var _girlfriendSprite = null;
    var _persons = null;
    var _exclamationMark = null;

    var _flashOverlay = null;

    var _stateChangeEvent = null;
    var _directionChagneEvent = null;
    var _personCollisionEvent = null;
    var _deathEvent = null;

    var _dodged = false;

    self.init = function(params) {
        _playerSprite = params.player;
        _girlfriendSprite = params.girlfriend;
        _dodged = false;
        _persons = params.persons;
        _vfxLayer = params.vfxLayer;
        _exclamationMark = game.add.sprite(0, 0, 'exclamation_mark');
        _exclamationMark.kill();

        _girlfriendSprite.animations.add('walk', [0, 1, 2, 3]);
        _girlfriendSprite.animations.add('idle', [0]);
        _girlfriendSprite.animations.play('walk', 18, true);
        //_vfxLayer.add(_exclamationMarks);

        _playerSprite.animations.add('idle', [0]);
        _playerSprite.animations.add('idle_dodge', [2]);
        _playerSprite.animations.add('walk', [0, 1]);
        _playerSprite.animations.add('dodge', [2, 3, 4, 5]);
        _playerSprite.animations.add('sit', [6, 7, 8, 9, 10]);

        for(var i=0; i < _persons.length; ++i) {
            _persons[i].animations.add('idle', [0, 1, 2, 3, 4, 5, 6, 7, 8]);
            _persons[i].animations.add('hit', [9, 10, 11, 12, 0]);
            _persons[i].animations.play('idle', 12, true);
        }

        _stateChangeEvent = EventManager.register("NEW_PLAYER_STATE", onStateChange);
        _directionChagneEvent = EventManager.register("DIRECTION_CHANGED", onDirectionChange);
        _personCollisionEvent = EventManager.register("PERSON_COLLISION", onPersonCollision);
        _deathEvent = EventManager.register("DEATH", onDeath);

        _flashOverlay = createOverlay();
        _flashOverlay.alpha = 0;
        _vfxLayer.add(_flashOverlay);

        startWalkAnimation("IDLE");
    }

    self.shutDown = function() {
        EventManager.remove("NEW_PLAYER_STATE", _stateChangeEvent);
        EventManager.remove("DIRECTION_CHANGED", _directionChagneEvent);
        EventManager.remove("PERSON_COLLISION", _personCollisionEvent);
        EventManager.remove("DEATH", _deathEvent);
    }

    function startWalkAnimation(key) {
        if(key === "IDLE") {
            if(!_dodged) _playerSprite.animations.play('idle', 18, true);
            else _playerSprite.animations.play('idle_dodge', 18, true);
        }
        else if(key === "RIGHT") {
            _playerSprite.scale.setTo(1, 1);
            if(!_dodged) _playerSprite.animations.play('walk', 18, true);
            else _playerSprite.animations.play('dodge', 18, true);
        }
        else if(key === "LEFT") {
            _playerSprite.scale.setTo(-1, 1);
            if(!_dodged) _playerSprite.animations.play('walk', 18, true);
            else _playerSprite.animations.play('dodge', 18, true);
        }
    }

    function onDirectionChange(params) {
        startWalkAnimation(params.direction);
    }

    function onPersonCollision(params) {
        params.person.animations.play('hit', 18, false);
        game.camera.shake(0.005, 400);
        if(_flashOverlay.alpha === 0) { 
        _flashOverlay.alpha = 0.5;
            game.add.tween(_flashOverlay).to({alpha : 0}, 250, Phaser.Easing.Cubic.Out, true);
        }
    }

    function onStateChange(params) {
        if(params.state === "SIT") {
            _playerSprite.animations.play('sit', 12, true);
        }
        else if(params.state === "WALKING") {
            _dodged = false;
            startWalkAnimation(params.walkDirection);
        }
        else if(params.state === "DODGE") {
            _dodged = true;
            startWalkAnimation(params.walkDirection);
        }
    }

    function onDeath(params) {
        var timeOffset = 250;
        _girlfriendSprite.scale.setTo(-1, 1);
        _girlfriendSprite.position.x += 32;
        _girlfriendSprite.animations.play('idle', 18, true);
        game.time.events.add(timeOffset, function() { 
            _exclamationMark.reset(_girlfriendSprite.world.x - 16 , _girlfriendSprite.world.y);
            game.add.tween(_exclamationMark.position).to( {y : _exclamationMark.position.y - 32}, params.switchTime-timeOffset, Phaser.Easing.Cubic.Out, true );
            game.add.tween(_exclamationMark).to( {alpha : 0}, params.switchTime-350, Phaser.Easing.Cubic.Out, true );
        });

    }

    function createOverlay() {
        var overlay = game.add.graphics(game.world.width, game.world.height);
        overlay.beginFill(0xffffff);
        overlay.drawRect(0, 0, game.world.width, game.world.height);
        var sprite = game.add.sprite(0, 0, overlay.generateTexture());
        return sprite;
    }
}
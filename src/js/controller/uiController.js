var UiController = function() {
    var self = this;

    var _uiLayer = null;

    var _playerModel = null;
    var _scoreModel = null;

    var _bar = null;
    var _barbg = null;

    var _scoreText = null;
    var _score = null;

    self.init = function(params) {
        _uiLayer = params.uiLayer;
        _playerModel = params.playerData;
        _scoreModel = params.scoreData;

        _bar = game.add.sprite(game.world.width - 32, 80, 'bar');
        _barbg = game.add.sprite(game.world.width - 32, 16, 'bar_bg');
        _bar.anchor.setTo(0, 1);

        _scoreText = game.add.bitmapText(16, 16, 'font', 'Score: ', 8);

        _score = game.add.bitmapText(64, 16, 'font', '0 ', 8);

        _uiLayer.add(_bar);
        _uiLayer.add(_barbg);
        _uiLayer.add(_scoreText);
        _uiLayer.add(_score);
    }

    self.update = function() {
        var percentage = _playerModel.power();
        _bar.scale.setTo(1, percentageToBar( percentage ) );
        if(percentage < 25) {
            _bar.frame = 1;
        }
        else {
            _bar.frame = 0;
        }

        _score.text = _scoreModel.score();
    }

    self.shutDown = function() {

    }

    function percentageToBar(percentage) {
        return Math.round( (64/100)*percentage );
    }
}
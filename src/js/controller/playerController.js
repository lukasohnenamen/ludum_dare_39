var PlayerController = function() {
    var self = this;

    var _speed = 0;
    var _powerReductionSpeed = 10;
    var _powerRegainSpeed = 20;
    var _model = null;

    var _sprite = null;
    var _bags = null;

    var _deathSwitchTime = 1500;

    var _upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
    var _upKey2 = game.input.keyboard.addKey(Phaser.Keyboard.W);
    var _downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
    var _downKey2 = game.input.keyboard.addKey(Phaser.Keyboard.S);
    var _leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
    var _leftKey2 = game.input.keyboard.addKey(Phaser.Keyboard.A);
    var _rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
    var _rightKey2 = game.input.keyboard.addKey(Phaser.Keyboard.D);

    var _state = "WALKING"; // DODGE, SIT
    var _walkDirection = "IDLE"; // LEFT, RIGHT
    var _currentUpdate = walkUpdate;

    // collision objects
    var _seats;
    var _persons;

    var _currentSeat = null;

    var _levelUpEvent = null;

    self.init = function(params) {
        _sprite = params.player;
        _bags = [];
        _seats = params.seats;
        _persons = params.persons;
        _state = "WALKING";
        _walkDirection = "IDLE";
        _currentUpdate = walkUpdate;
        if(!_model) {
            _model = new PlayerModel();
        }
        _model.setPower(100);

        var speed = Progress.speed(1);
        _speed = speed * 1.5;
        _powerReductionSpeed = Progress.powerReduction(1);
        _powerRegainSpeed = _powerReductionSpeed * 2.5;

        _levelUpEvent = EventManager.register("LEVEL_UP", onLevelUp);
    }

    self.shutDown = function(params) {
        EventManager.remove("LEVEL_UP", _levelUpEvent);
    }

    self.update = function() {
        _currentUpdate();

        if(_state !== "SIT" && _state !== "DEATH") {
            _model.setPower( _model.power() - _powerReductionSpeed * (game.time.elapsedMS / 1000) );
        }

        if( (_sprite.position.x < -7 || _model.power() <= 0) && _state !== "DEATH") {
            EventManager.trigger('DEATH', { switchTime : _deathSwitchTime });
            changeState("DEATH");

            game.time.events.add(_deathSwitchTime, function() { game.state.start("ENDCARD"); });
        }
    }

    self.getModel = function() {
        return _model;
    }

    function walkUpdate() {

        move(1);

        personCollision();

        if (_downKey.isDown || _downKey2.isDown)
        {
            var currentSeat = checkSeat();
            if(currentSeat !== null) {
                _sprite.position.x = currentSeat.position.x + 16;
                _currentSeat = currentSeat;
                changeState("SIT");
            }
            else {
                changeState("DODGE");
            }
        }
    }

    function move(multiplicator) {
        if (_leftKey.isDown || _leftKey2.isDown)
        {
            _sprite.position.x -= Math.round( (_speed * (game.time.elapsedMS / 1000) / 2)*multiplicator );
            if(_walkDirection !== "LEFT") {
                _walkDirection = "LEFT";
                EventManager.trigger("DIRECTION_CHANGED", {direction : "LEFT"});
            }
        }
        else if (_rightKey.isDown || _rightKey2.isDown)
        {
            _sprite.position.x += Math.round( (_speed * (game.time.elapsedMS / 1000))*multiplicator );
            if(_walkDirection !== "RIGHT") {
                _walkDirection = "RIGHT";
                EventManager.trigger("DIRECTION_CHANGED", {direction : "RIGHT"});
            }
        }
        else {
            if(_walkDirection !== "IDLE") {
                _walkDirection = "IDLE";
                EventManager.trigger("DIRECTION_CHANGED", {direction : "IDLE"});
            }
        }
        
    }

    function checkSeat() {
        for(var i=0; i < _seats.length; ++i) {
            if(_seats[i].alive) {
                var boundsA = _seats[i].getBounds();
                var boundsB = _sprite.getBounds();

                if(Phaser.Rectangle.intersects(boundsA, boundsB)) {
                    return _seats[i];
                }
            }          
        }
        return null;
    }

    function sitUpdate() {
        _model.setPower( _model.power() + _powerRegainSpeed * (game.time.elapsedMS / 1000) );
        _sprite.position.x = _currentSeat.position.x + 16;
        if (_upKey.isDown || _upKey2.isDown)
        {
            changeState("WALKING");
        }
    }

    function dodgeUpdate() {
        move(0.6);

        if (_upKey.isDown || _upKey2.isDown)
        {
            changeState("WALKING");
        }
    }

    function deathUpdate() {

    }

    function personCollision() {
        for(var i=0; i < _persons.length; ++i) {
            if(_persons[i].alive && !_persons[i].playerCollision) {
                if(_sprite.position.x + 8 > _persons[i].position.x + 8 && _sprite.position.x + 8 < _persons[i].position.x + 24 || 
                   _sprite.position.x - 8 > _persons[i].position.x + 8 && _sprite.position.x - 8 < _persons[i].position.x + 24 ) {
                    _model.setPower( _model.power() - 20 );
                    EventManager.trigger("PERSON_COLLISION", {person : _persons[i]});
                    _persons[i].playerCollision = true;
                }
            }
        }   
    }

    function changeState(key) {
        if(key === "SIT") {
            EventManager.trigger("NEW_PLAYER_STATE", {prevState : _state, state : "SIT"});
            _state = "SIT";
            for(var i=0; i < _bags.length; ++i) {
                _bags[i].position.x = game.rnd.integerInRange(-32, 0);
            }
            _currentUpdate = sitUpdate;
        }
        else if(key === "WALKING") {
            EventManager.trigger("NEW_PLAYER_STATE", {prevState : _state, state : "WALKING", walkDirection : _walkDirection});
            _state = "WALKING";
            _currentUpdate = walkUpdate;
            _currentSeat = null;
            for(var i=0; i < _bags.length; ++i) {
                _bags[i].position.x = -16;
            }
        }
        else if(key === "DODGE") {
            EventManager.trigger("NEW_PLAYER_STATE", {prevState : _state, state : "DODGE", walkDirection : _walkDirection});
            _state = "DODGE";
            _currentUpdate = dodgeUpdate;
        }
        else {
            _state = "DEATH";
            _currentUpdate = deathUpdate;
        }
    }

    function onLevelUp(params) {
        // change walk speed
        var speed = Progress.speed(params.level);
        _speed = speed * 1.5;

        _powerReductionSpeed = Progress.powerReduction(1);
        _powerRegainSpeed = _powerReductionSpeed * 2.5;

        // add bag
        var bag = game.add.sprite(-16, 0, 'bag', game.rnd.integerInRange(0,5));
        _sprite.addChild(bag);
        _bags.push(bag);
    }
}
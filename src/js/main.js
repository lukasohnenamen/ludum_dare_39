// params: width, height, rendertype, parentId, default state, transparency, antialias, physics
var game = new Phaser.Game(512, 320, Phaser.AUTO, '', null, false, false, null);

// add game states
game.state.add('BOOT', Boot);
game.state.add('LOAD', Load);
game.state.add('TUTORIAL', Tutorial);
game.state.add('PLAY', Play);
game.state.add('ENDCARD', Endcard);

// start loading gamestate 
game.state.start('BOOT');
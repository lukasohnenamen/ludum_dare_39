WebFontConfig = {
    active: function() { game.time.events.add(Phaser.Timer.SECOND, function(){ game.fontReady = true; }); },
    google: {
      families: ['Press Start 2P']
    }
}

var FontStyle = {
    font : '16px Press Start 2P',
    fill: '#ffffff'
}
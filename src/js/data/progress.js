var Progress = {
}

Progress.speed = function(level){
    return 2 * (level*level) + 100;
};

Progress.powerReduction = function(level) {
    return 2 * level + 10;
}

Progress.nextSeatTimeMin = function(level) {
    return -1 * level + 1300;
}

Progress.nextSeatTimeMax = function(level) {
    return -1 * level + 3800;
}

Progress.personSpawnMin = function(level) {
    return -25 * (level * level) + 3600;
}

Progress.personSpawnMax = function(level) {
    return -25 * (level * level) + 6200;
}

Progress.scoreMultiplicator = function(level) {
	return level * 0.2 - 0.1;
}
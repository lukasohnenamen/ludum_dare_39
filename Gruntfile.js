module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*\n * <%= pkg.name %> \n * author: <%= pkg.author %>\n * version:<%= pkg.version %> [<%= grunt.template.today("yyyy-mm-dd") %>]\n */\n'
      },
      build: {
        src: [
            'src/js/data/*.js',
            'src/js/util/*.js',
            'src/js/model/*.js',
            'src/js/controller/*.js',
            'src/js/states/*.js',
            'src/js/*.js'
        ],
        dest: 'app/js/game.min.js'
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);

};